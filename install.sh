#!/bin/bash
path=~/Projects/itential/iap/repos

# Function to clone and install dependencies
clone_and_install() {
    local repo_url=$1
    local version=$2
    local repo_name=$(basename $repo_url .git)
    local temp_dir="$path/$repo_name"

    # Clone the repository into the temp_repos folder
    if ! git clone $repo_url $temp_dir; then
        echo "Failed to clone repository: $repo_url"
        return
    fi

    # Move to the repository directory
    cd $temp_dir || {
        echo "Failed to change directory to $temp_dir"
        return
    }

    # Check if the specified version exists
    if git rev-parse --verify --quiet $version > /dev/null; then
        # Checkout the specific version
        git checkout $version
    else
        echo "Version $version not found in repository $repo_name"
        cd $path
        return
    fi

    # Install npm dependencies
    npm install || {
        echo "Failed to install npm dependencies for repository: $repo_name"
        cd $path
        return
    }

    # Move back to the original directory
    cd $path
}

# Read package.json and parse dependencies
dependencies=$(jq -r '.dependencies | to_entries[] | "\(.value) \(.key)"' blueprint.json)

# Create the repos directory if it doesn't exist
mkdir -p repos

# Loop through each dependency
while IFS= read -r dependency; do
    # Split the line into package name and version
    read -r version name <<< "$dependency"

    # Extract repository URL from npm registry
    repo_url=$(npm view $name repository.url | sed 's/^git+//')

    # Clone and install the dependency
    clone_and_install $repo_url $version
done <<< "$dependencies"