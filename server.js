#!/usr/bin/env node
/* eslint
import/no-extraneous-dependencies:off,
import/no-unresolved:off,
no-unused-vars:off,
*/
const Server = require('@itential/pronghorn-core');
const fs = require('fs');

// TODO: isEncrypted is now redundant and can be pushed down into core
const pronghornProps = JSON.parse(fs.readFileSync('properties.json', 'utf-8'));
const isEncrypted = pronghornProps.pathProps.encrypted;
const myServer = new Server(__dirname, 'properties.json', isEncrypted);
